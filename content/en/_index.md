+++
archetype = "home"
title = "Radical Security"
+++

{{% notice style="warning" title="Hide yourself" icon="user-ninja" %}}
If you are reading this website, you might be an activist/militant involved in radical work and action. State surveillance exists and it is advised that you see this website using [Tor Browser](https://www.torproject.org/) or [TAILS](https://tails.net/index.en.html).
{{% /notice %}}

## What is Radical Security?

**Radical Security** is a project created to aggregate and produce new knowledge for Radical (*as in to the root of the problem*) Activists, Militants and Organizers.

It appeared from the necessity of creating content that would encompass multiple threat models and at the same time be realist on what are the risks. The current online mainstream guides are not enough for us (from the ones made for Investigative Journalists to those made to Human Rights Defenders)

## How to navigate this website

There are 3 main sections:
1. [Security 101](/security-101)
    - *(Where you can learn the basis of Digital and Physical Security, alongside some of the common terms used)*
2. [Operational Security](/opsec)
    - *(Where you can learn and see context-specific implementations of Operational Security)*
3. [Organizational Security](/orgsec)
    - *(Where you can learn how to create a working and transformative Security Culture inside your organization, as well as implementations and reflections on Organizational Security)*

There is also an [About](/about) section, that includes how can you contribute, who is behind this project, other collectives and resources that can support you.
