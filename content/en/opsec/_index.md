+++
archetype = "chapter"
title = "Operational Security"
weight = 2
+++
*Scroll down to find the list of articles in this chapter*

Articles, reflections and general tips on how to create, maintain and understand Operational Security (OpSec) for Radical Activists.

## What is OpSec

**Op**erational **Sec**urity is the process/capacity of:
1. Identifying *critical information* for the success of a specific mission/action;
2. Analyzing how does the **enemy** (Companies, Private Security Companies, Far-Right, Police, MIlitary and/or Governments) can get access to the *critical information*;
3. Applying counter-measures to our vulnerabilities;

This is the process that allows us to keep on fighting for a just world without being stopped. It is not focused on exclusively avoiding arrest/identification/imprisonment, it is focused on increasing the chances of success of your action.

## Chapter Articles

List of posts and guides under this chapter:

{{% children sort="weight" %}}
