+++
title = "Phone Security"
weight = 1
tags = ["opsec", "communication"]
+++

Cellphones are one of the basis of the modern world, they allow us to communicate our messages, recruit new members, get information fast and connect with far away comrades. At the same time, phones are the easiest surveillance device that we carry daily, creating metadata about our daily routine and mapping our acquaintances.

This article is separated into 3 sections:
- [Day to Day Security](/opsec/phone-security/#day-to-day-security);
- [Before an Action](/opsec/phone-security/#before-an-action);
- [Emergency Situations](/opsec/phone-security/#emergency-situations);

We will also dive into some application alternatives (all [open source](/security-101/building-security-practices/#open-sourcing-security) and actively maintained).

## Day to Day Security

### Operating Systems

Before delving into digital security practices, we wanted to present our analysis on cellphone Operating Systems.

The 2 major ones are `iOS` and `Android`. There are many reasons why someone would use one or another and while we defend Open Source software as a practice, iOS devices are used by many activists and we cannot ignore that they also need to harden. In general we suggest the usage of a private-centered Android Distribution (such as [Graphene OS]()) but we will have security advice for either stock Android and iOS in this article.

### Encryption

The first thing you should do is enabling **Encryption** on your device. This will protect your phone's contents when turned off, creating a bigger work for police to access your device in case it gets seized.

On iOS:
1. Update your iOS version by:
  - Going to the `Settings` app;
  - Selecting `General` in the menu;
  - Going to the `Software Update` section;
  - If there is any version that you can update to, do it;
2. Enable Encryption by enabling Passcode:
  - Go to the `Settings` app;
  - Create a good passcode (*you might need to select `Passcode Options` to switch from the standard 6 digit code to a custom alphanumeric code*)

> You should deactivate Touch ID & Face ID since those are easier to crack (the police might just point it to your face our your finger).

On Android:
1. Create a password:
  - Go to the `Settings` app;
  - Find the `Security` section;
  - Select `Screen Lock` and create a `Password` (*mix between numbers and letters, minimum of 8 digits recommended*);
2. Check if Encryption is enabled:
  - Go to the `Settings` app;
  - Find the `Security` section;
  - Select `Encryption`;
    - If phone is Encrypted do nothing;
    - If phone is not encrypted, while charging, select `Encrypt`;

> You should not use Fingerprint or Facial features to unlock the phone.

{{% notice style="warning" title="Encryption Caveats" icon="circle-exclamation" %}}
Encryption only works before the first time you input your password when booting your phone.
If you want to "activate it" turn off your phone.

If you get arrested and do not have your phone with you, try to make someone turn it off.
{{% /notice %}}

### Installed Applications

1. Reducing the number of applications you use is a practice advised, each app that you have expands the attack vector that Law Enforcement or Private Companies might exploit to get access to your cellphone;
2. Be wary of the permissions you give to Applications (especially `Media Access`, you can refuse to give them that access and when you want to share something on that app you go to the File Manager and share the specific file/image with the application);
3. See the [end](/opsec/phone-security/#application-alternatives) of this article to find Open-Source and Secure alternatives to everyday apps;
4. Keep all the applications updated (daily);

> You can have secure a secure browser, secure messaging and "private" social media but do not forget to install a Secure and preferably Open-Source Keyboard.
>
> Our Suggestions are:
> - For iOS - use the default keyboard with telemetry (analytics) turned off;
> - For Android - **do not use** the default keyboard (most of the times filled with telemetry and data-grabbing), you might try:
>   - [AnySoftKeyboard](https://anysoftkeyboard.github.io/);
>   - [OpenBoard](https://github.com/openboard-team/openboard);
>   - [Simple Keyboard](https://github.com/rkkr/simple-keyboard);
>   - [FlorisBoard](https://florisboard.org/) (still in beta);

### Connecting and Using the Internet

The main reason why we need smartphones is the capacity to connect to the internet. There are a number of threats that we need to be on the look out for when doing this, namely:
- Your connection might be unsafe (your logins and what pages you access can be changed/recorded);
- Your location might be exposed;
- The device you are using might be exposed;
- The downloads you make might be malicious;
- You might access malicious websites;

To deal with this you should:
- On **Public** Networks, always use a VPN (see our recommendation in [the end](/opsec/phone-security/#application-alternatives));

> The usage of a **Virtual Private Networks** (VPN) creates an encrypted tunnel between your device and the VPN Server (that you should trust for this to work), making the connection inside the Public Network.

- Activate `HTTPS`, do not connect to websites using `HTTP`;
- If you want to hide your location, either use a VPN or use [Orbot](https://orbot.app/en/) / [Tor Browser](https://www.torproject.org/download/);
- To hide what your device is use [Tor Browser](https://www.torproject.org/download/);
- Always be careful on what you download into your device, try to open documents on the browser and keep your device activated;
- Do not accept permissions (`Download`, `Media Acess`, `Location`, `Camera`, `Microphone`) from websites you do not regularly use/trust;

> **About Tor Browser and Orbot.**
>
> Tor Browser is a Web Browser (just like Firefox/Chrome...) that comes already setup with Fingerprinting Resistance and other technical protections. Besides those protections, it is used to connect to Tor network, a network made by thousands of servers and relays that can hide to what website your are connecting to (from your perspective and from the website's perspective).
>
> Orbot acts as a VPN on the Tor Network, where your web-request s trough 3 other servers before connecting to the website/service, goes trough 3 other servers before connecting to the website/service, anonymizing your connection and protecting it from snooping.

### Secure Communication 101

Firstly, SMS and Phone Calls are **always** insecure, they can be easily intercepted and modified by the State and you should not use them to contact comrades. Besides the content (data), using SMS will mean that our enemies can paint a map of our networks and militants (metadata).

Secondly, closed-source applications (such as *Whatsapp*) are generally untrustworthy due to the fact that we cannot check the actual code of the applications

Thirdly, `Telegram` is not secure by default even though they try to market themselves as such. There is no encryption in group chats and the end-to-end encryption on direct messages has to be activated individually for each chat. Telegram also states in their [Privacy Policy](https://telegram.org/privacy) that if they recieve a Court Order confirming that you are a Terror Suspect they will disclose your IP Address (location) and Phone Number. It is not an uncommon tactic for enemies of the socio-economic system to be painted as terrorists (see [Alfredo Cospito](https://en.wikipedia.org/wiki/Alfredo_Cospito) in 41 bis).

Finally, our array of suggestions on instant messaging:
- Signal / Molly (*see our [Signal Hardening] article*);
- [Briar](https://briarproject.org/);
- [Cwtch](https://cwtch.im/);

> To understand Briar and Cwtch, and why you might prefer them to Signal, you can read the article called [The Guide to Peer-to-Peer, Encryption and Tor](https://www.notrace.how/resources/download/the-guide-to-peer-to-peer-encryption-and-tor/the-guide-to-peer-to-peer-encryption-and-tor-read.pdf).

## Before an Action

Our main recommendation, synchronized with many radical groups and activists recommendation, is to leave your phone at home.

Bringing it to the action leaves a digital trail that can connect you with whatever happens there. Bringing it will also mean that it could eventually be seized.

A simple (but possibly expensive) solution is to get a temporary phone (either dumb or smart (if you need to take photographs (aka you are part of a Media Team))).

Besides that, here are some of the main things you should do before leaving your house (with or without your cellphone).

### Cleanup

*Purpose: removing information that might put you or others in danger.*

#### Cellphone

- Make a list of photographs, documents, guides, contacts (...) that you have on your phne;
- Copy them to an encrypted flash drive / laptop;
- Delete them from your phone;

#### Signal
*Multiple options, depending on threat level.*

- Uninstall Signal;
- Delete all the messages (`Signal > Settings > Storage > Delete Message History`);
- Exit all groups, delete "Direct Messages" and delete "Notes to Self";

> You might mix and match all of these options as well.

#### Contacts
*Imagine if the police gets access to your phone.*

- Do not save contacts as "Name - Collective X" / "Name - Action Y". Rename those that were written in this format.

#### Social Media

- Logout of all Social Media accounts you do not need;
- Logout of Social Media accounts from other collectives you are part of;
- Do the same with E-mail accounts;

#### Browser

- Delete the browser and internet history;

### Lockdown

*Purpose: creating more barriers to protect yourself / your device.*

#### Cellphone

- Deactivate Biometrics (Face ID, Fingerprint ...);
- Create a good password (8-12 numbers and letters);
- Uninstall applications you do not need during the action (you can reinstall them afterwards);
- Confirm if you have your files Encrypted (see how [here](/opsec/phone-security/#encryption));
- Update your phone (applications and operating system);
- Reboot (*most mobile malware doesn't survive a reboot*);

#### Signal

- Follow our [Signal Hardening](/opsec/phone-security/hardening-signal) article;

## Emergency Situations

There are some scenarios where you should plan ahead what would you do. In times of stress (emergency) it is much harder to think thoroughly therefore you need to have specific protocols on what to do individually and collectively.

- What happens in case there is a raid/arrest?
  - How will you communicate this with other people?
  - What needs to happen while you are arrested (are there collective accounts that need their password changed (...)?
  - You should not use that phone anymore (especially if it was separated from you), how will you communicate with your comrades in the next months?
  - What will you do and how fast can you do it? (*It is a good security policy to uninstall Signal and to shutdown your phone (or even Factory Reset it)*)
- How will you find spyware / what will you do in case of suspicion?
  - Do you have any alternative devices?
  - How could you warn your comrades to not message you?
  - Is there any support group that can help you? You might want to check out the [Collectives page](/about/collectives).

## Application Alternatives

| Current Application / Service | Alternative(s) | Notes |
| --- | --- | --- |
| Whatsapp | [Signal](https://signal.org/) / [Molly](https://molly.im/) | You can also check the [PET guide](https://www.notrace.how/resources/download/the-guide-to-peer-to-peer-encryption-and-tor/the-guide-to-peer-to-peer-encryption-and-tor-read.pdf) |
| Proprietary VPNs | [Calyx VPN](https://calyxinstitute.org/projects/digital-services/vpn) / [Riseup VPN](https://riseup.net/en/vpn) | Donate to the projects if you are able to. |
| Slack / Discord | [Matrix](https://matrix.org/) ([Element](https://element.io/) / [Syphon](https://syphon.org/)) | *Syphon in Open Alpha and not considered ready for everyday use* |
| E-mail App | [K-9 (Android) ](https://k9mail.app/) |

You can find more alternatives [here](https://degooglisons-internet.org/en/).

## Further Reading

- [ ] [The guide to Peer-to-Peer, Encrypted and over Tor communication](https://www.notrace.how/resources/download/the-guide-to-peer-to-peer-encryption-and-tor/the-guide-to-peer-to-peer-encryption-and-tor-read.pdf);
- [ ] [Turn Off Your Phone! And other basic digital security strategies](https://www.notrace.how/resources/download/turn-off-your-phone/turn-off-your-phone-read.pdf);
