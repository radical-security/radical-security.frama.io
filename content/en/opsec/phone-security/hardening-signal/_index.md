+++
title = "Signal Hardening"
weight = 1
tags = ["opsec", "communication"]
+++

Signal is already very secure by default, but there are specific settings you might want to tinker to et the most of the secure messaging app.

## Registration

The biggest downside of Signal is the need for a valid and active phone number. Most people use their personal registered number but if you want to completely compartmentalize your personal life with your anticapitalist militancy, you can get a temporary (Burner) SIM card. To do this, you will need to:
1. Get cash, do not pay with a Debit/Credit Card;
2. Go to another zone in your city;
3. By a cheap burner SIM (in some places you can get them in mini-markets);
4. Activate it far away from your home, connect it to Signal and dispose the SIM Card;

## Lock Screen protections

If someone gets access to your unlocked cellphone, they can instantly read your Signal messages. To mitigate it we want to activate `Screen Lock`. To do so you will:
1. Open Signal;
2. Go to `Settings` (clicking on your Profile Image);
3. Scrolling to `Privacy`;
4. Find `App Security`;
5. Activate `Screen Lock`;

> Do not use patterns to unlock your phone, as they are generally insecure.
>
> The Screen Lock will be the same as your Phone's Screen Lock. Molly (a Signal Fork) implements a passphrase to protect your messages (separate from the Phone's Lock Screen).

## Registration Lock

If someone gets access to your SIM card, they could just connect to Signal and have access to all the groups and future messages that you would receive (especially if you bought a temporary SIM card that will expire). To address this:
1. Open Signal;
2. Go to `Settings` (clicking on your Profile Image);
3. Scrolling to `Account`;
4. Selecting `Registration Lock`;
5. Choosing a PIN and inputting it into a Password Manager to save it;

## Incognito Keyboard

Many keyboards (especially in Android devices) store what you wrote in order to train the algorithm. In order to avoid this:


## Message Destruction

Signal allows you to:
- Destroy single messages (just for you);
- Destroy single messages (for everyone in the group);
- Automatically destroy messages after a certain time that they were  in one group;
- Automatically destroy messages after a certain time that they were read for every group you create;
- Destroy all the messages and groups you are in;

To destroy **single messages**:
1. Select (long press) the message you want to destroy;
2. Click on `Delete`;
3. Select `Delete for me` or `Delete for everyone`;

To **automatically** destroy **messages in a group/conversation**:
1. Open the conversation;
2. Click on the conversation's icon;
3. Select `Disappearing Messages`;
4. Select the time frame (how much time after someone has read the message will it auto-delete);

> By default between 1 and 4 weeks is acceptable, for any information that might put you or other people in risk we recommend maximum 1 day.

To **automatically** destroy **messages in a group/conversation you start**:
1. Open Signal;
2. Go to `Settings` (clicking on your Profile Image);
3. Scrolling to `Privacy`;
4. Find `Disappearing Messages`;
5. Select the time frame (how much time after someone has read the message will it auto-delete);

To destroy **Everything**:
1. Open Signal;
2. Go to `Settings` (clicking on your Profile Image);
3. Select `Account`;
4. Select `Delete Acount`
5. Re-register (*optional*);

## Blocking Screenshots

To deactivate Screenshots in Signal, you should:
1. Open Signal;
2. Go to `Settings` (clicking on your Profile Image);
3. Scrolling to `Privacy`;
4. Find `App Security`;
5. Select `Screen Security`;

## Exiting and Deleting groups

After an action(s) or any other reason, you might want to exit the groups you were in and delete the remaining contents. This is used to protect you and comrades and even though there shouldn't be sensitive content (due to message destruction) on the group, you could put people at risk just by having a group with them. Therefore, to do this, you will:
1. Open Signal;
2. Select the conversation you want to exit;
3. Click on the conversation's icon;
4. Select `Leave group`;
5. In the message navigation, select (long press) the conversation you just exited;
6. Click on `Delete`;
7. Do the 5th and 6th step in every device you are connected with Signal (it exits from the group but you have to manually delete the remaining contents in each one);

## iOS and Calls

Signal allows history access to the Phone app in iOS. If you have iCloud it will also share it with iCloud. The history includes **with whom did you talk**, for **how long** and **when**. To deactivate this:
1. Open Signal;
2. Go to `Settings` (clicking on your Profile Image);
3. Selecting `Privacy`;
4. Deactivating `Show Recent Calls`;
