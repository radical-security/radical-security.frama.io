+++
archetype = "chapter"
title = "About"
weight = 4
+++

{{% notice style="warning" title="About" icon="clipboard-question" %}}
In this section you can find:
- What is the project;
- How to contribute to it;
- Who is behind Radical Security;
- Other Collectives and Support
{{% /notice %}}

## What is it?

Radical Security is a project run currently by 1 activist, security trainer and security officer with the alias Larus Argentatus (if you want to contribute, don’t be afraid to reach out). It was created to aggregate and produce new knowledge for Radical (anti-capitalist) Activists, Militants and Organizers.

## Who is behind Radical Security?

Currently run by 1 activist (*Larus Argentatus*), biography below.

{{% expand title="*Larus Argentatus*" %}}

Hey, I am *Larus Argentatus* (*aka* António), an anti-capitalist militant that has been to plenty of different countries to participate and engage in conferences, camps, debates and actions.

I am based in Europe, organized mostly around Climate Justice. I am also a *Security Trainer*, *Security Officer* and a *Cybersecurity Student/Apprentice*.

I am on Mastodon ([@infosec.exchange@larusargentatus](https://infosec.exchange/@larusargentatus)) and you can also send me an e-mail `larus-argentatus [at] riseup.net`.

{{% /expand %}}

{{% expand title="*Larus Argentatus* - PGP Key" %}}

You can also download it [here](/pgp/larus-argentatus.asc):

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

xsFNBGTspCsBEADlP2JIZYzL2vUad5pIm1qhBIFIKi+HQv2DXbLvlSpJIuTy/aza
/U5CYK9456/JjpQ7MDzGdtv1TWIWDEMM9erztcdUA9n1BI7AVa+E0eJtQPoat5EQ
8Ihw6fmUnAWyWD1/M8RblK3NzqCfXO2dyXmBXGEQk7b2tJz+d2Tmtf35fnkTiNXb
CCmOiwtb0PJxoUTNnumCbm6ngIlQ1rOQwoccQfU7R2b0AeUZwtnwM+tyi0INyyWd
73LaDkBx+p4u9475kcDBRWgiEC3fK1D2nuG2370LCZ8rOdHZL8mMfloYaaUgvCaf
7/wokpNOkNk//ZglQZoBDRnDgE9aNfwSZonV7FadWZaB+DfdW+xjQyLWrWQkIOzL
al3FmBCSfTLvsuwIn3RSZ/Z7u/ut2Ko1aD0g2yjefZamBzQZ9MeGVCunWTOS0ml7
R0Pv7VyDmcMYqry7urOfNLIFQd5BUIOHguG0QYu5JP3Tc/EfMpLpp2OvhSfk394i
p1fkOWOu8B6odCSSY7xePmiyKH7+LnrfIOvbJ5y77Zbd7XQGckxMLtRYSMRSYNkl
kH1B0rCWO8EB8/aJJArEXkkZ7H+Ii1zqzExkRFIfIJBTQRGfXYoISBCIH5Un+xAo
yxLvb5so2vmiKX8dEGLkfSovc8GUXbY4nUUhXFWaMMoJZ0M6E87fmmlYWwARAQAB
zS5MYXJ1cyBBcmdlbnRhdHVzIDxsYXJ1cy1hcmdlbnRhdHVzQHJpc2V1cC5uZXQ+
wsGHBBMBCAAxFiEEdSattXCFRFuTr6aThtiaVdYC37IFAmTspC4CGwMECwkIBwUV
CAkKCwUWAgMBAAAKCRCG2JpV1gLfsnuED/0Re568SXas6WcIstCwRrmFwQw2K5BT
NM+JpTh7Km2i151EsDZi7aI47wXLt2D6WSuwjtlv/HC6ZOoo9jgwRQZ5qkSAdQz1
D567kywhRig5xtLxkAMJoGfyNzK9CV9p+JeMJvF92/ATQ7f0Ifb1YAZfx76JZWum
FQRLO2y+QIYPd1IEJqgId1O8ttVpvb/7fFZrOqwKfzjmrpP1JkY33J/QZuLQiPwP
W3KQuo/2GXxjJeWLPhSTXS17n7sGrVKCTnAefxMxJgRgFeeiPUfGR/uyelIG6f5S
2fs4yx1khxTtVW3jq7OnmqXEPUigfSgO71Rp/uIs5IN+I0E/2dw1qpB2lQzl7v2K
TIVelUIi3cA8hlBHJxqhpnytyUQ8+w+MGrzjPJa2jPgMxVru3dc0GbNNVauViX/D
ftEaoky1yGPmekvhvwCSRCzwaiutI4keY3Q0qKNz4tq80G3Xf0kgVhwBNRY/cWvy
Tu3rXtKEoonW/vVLLtAT6DQhCsZw58/uWFCJcWb4JxHGaoMrY3Gb/ZSBw5afhYZn
1MANqQ02ha7Lyr25rc0YOsVvYnGIHeGS+hGbDr0035ImwUD2opGGT0M7WGeB09dH
8fa1UAOwVkaYRj8VTcnMQ81nSTNqUHrbqqnNiE+qOl9lyEgzE4MOSQCAOBTYPKVk
keQZP7S807o4Xc7BTQRk7KQuARAAxPjDyIA0vK3lIw09MobELF1Rk/+YbSWx07E9
jw7ZkNbCF4iJUY04xjkGumYZietOIwaFteKBczGgO+l1mQH4bF0fhK5qYOi6fF9h
5mLG3nxEEuoCLuvRo5Qt7G0f56fQfO/UfZssS9g5JAMBJwoIm1ueYIPCrYBmVjTp
Whba0gL0Be2bZqMgW/IWDN6BcZ0UeXLNml+uUx/Ov6PW+040vVJxgt9Tyhll1q8b
GI7Hjo3PZBMDTj70eMssYc9XfHQ1hSC4bnvD5Nf1QzBOLGqlX1dTxfcC5BIV18ne
5MB/Vr+6Vr2jBRMRXA/La8LePLTZpWs5kYDbRr5W6XFyV4I46Yp3dINKfgUl79qQ
F1YV8uWMGBdyca1771pnxBdz6W46+sd393xbNBumTC5O3NWCVTiz4w90U90Y83gt
EwjMQaK8RCte7B+8Id91lExx0ADj9e4KU9wcPXcVfSA6OHBEWT4VzC3or6lvDPKb
3ChbjMv819fT3AjImPdK7oecFkcvKmcrzNaTGNVLocyMq5DtKL7Ci5aRK+Juf1oP
f1Y0H0eRU45AHOiMuoddXRFzfvkC7f0+NNViDWhU3oR+6Q61KM3V+L80J6xdddM+
dE15MjRKFLdSb60UMbqNiRcy9JZkYwinVcQUdmVYi1GRtc0jpQTThcy0nNuYWFwV
sSyxmfEAEQEAAcLBdgQYAQgAIBYhBHUmrbVwhURbk6+mk4bYmlXWAt+yBQJk7KQw
AhsMAAoJEIbYmlXWAt+y1xkP+QFFyQlXLCnFdgQAR8DFM5xDNN828wkEWJIu9X+L
Bu25HPnif5y83yyYjXE+oaSjvrBNnIkG/3DaOw5DAK+kYU84xIL1f8vVJ+HuSGgM
S6IPQbHMFkUN3vH6/It0x2w4vTx7nqXKAJbnUcOVeDWvdnFqYcvdyD2I0Volo51J
TNXmzPs/gJC/VeOMuw82BY5iwP9NkA3NLaKG5fMFvKscZ6SJWf3CfOQT9I5GecZ7
Yno3lzxEHsYcaRG1msK9TnUrLDSRkUTYHFBEHn+2tcCCgPt8YJkWbBTMNB2gIbHb
a0BMBsRwd32DLYW5s4Rc6o0au3aq74FC113OIQPKCQeSQuf8INgJ5bHv2+vmzO/h
A0+KNcAUFa33+/tkzADXfVwM71ayBJYUHJwoRFFbP5KuqoDdcsO5KURMEmo/iW6G
htXlMYMYGg18ZvzRhysLC5mZkZQK2FF9B/aQ7sP17JbDX1JO2ayx2cKLayJBS9Ge
UvzEvIoiXpcz13cycAvUxqrZxgV4uJpPDSNOdow846fqgo27A+1IXhUSh+bydvLY
r0+0c7SdPB6NFyPUKFfaEOn64QV7HPOxOCzywAWC+X4dvkwzsMz+IzMMy7tqDm0w
n8y9wQhAyo0ztOZ5ugMK62w7NZSUfd63HY7mF7y9t4yIb8P1FVbeNdkV8pYEFt8Z
ed/3
=aW6D
-----END PGP PUBLIC KEY BLOCK-----
```
{{% /expand %}}

---

## How can you contribute

There are many ways to contribute and I hope that there is one for you.

### Create new content

We are actively seeking for new content, new ideas and additions to the current content. To do that:

1. Install [git](https://git-scm.com/downloads);
2. Clone this repository `git clone https://framagit.org/radical-security/radical-security.frama.io.git`;
3. Add this repository to your git connection `git remote add <NAME> https://framagit.org/radical-security/radical-security.frama.io.git`;
4. Add, or alter the content inside `/content`;
5. Commit your changes and explain them `git commit -m "<EXPLANATION>"`;
6. Create a [framagit](https://framagit.org) account;
7. Push your changes to be approved `git push --set-upstream <NAME> main`;

You can also send suggestions by e-mail at `radical-security [at] riseup.net`.

### Ask for specific content

You can ask for specific content by opening an `Issue`.

1. Create a [framagit](https://framagit.org) account;
2. Open an `Issue` on the [Issue](https://framagit.org/radical-security/radical-security.frama.io/-/issues) tab;
3. Describe what content do you want, give as many information as possible;

You can also send requests by e-mail at `radical-security [at] riseup.net`.

### Share it

Share the content with comrades and compas. The maintainer is also in Mastodon.

### Send your feedback

Send it to `radical-security [at] riseup.net`.
