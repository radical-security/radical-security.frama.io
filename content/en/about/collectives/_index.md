+++
title = "Collectives and other Projects"
+++

There are many support, infrastructure, training and funding collectives and organizations that either support or might support (depending on the context) Radical Activists and Organizations. Here are some of them.

|Collective/Organization|Website|Categories|Adden in|
|:---|---|---|---|
| Access Now | [www.accessnow.org](https://www.accessnow.org/) | Emergency Support | October 2023 |
| CiviCert | [www.civicert.org](https://www.civicert.org/) | Emergency Support | October 2023 |
| CryptoHarlem | [www.cryptoharlem.com](https://www.cryptoharlem.com) | Research // Training | October 2023
| Digital Defenders Partnership | [www.digitaldefenders.org](https://www.digitaldefenders.org) | [Training](https://www.digitaldefenders.org/digital-protection-accompaniment/) // [Funding](https://www.digitaldefenders.org/funding/) | October 2023 |
| Holistic Protection Collective | [www.holistic-protection.eu](https://www.holistic-protection.eu) | Connecting // Training // Research // Consultancy | October 2023 |
| Lifeline | [www.csolifeline.org](https://www.csolifeline.org/) | Emergency Support // Funding | October 2023 |
| Nikau | [nikau.io](https://nikau.io) | [Training](https://nikau.io/services/training/) // [Auditing](https://nikau.io/services/audits) // [Coaching](https://nikau.io/services/opsec/) // [Infrastructure](https://nikau.io/services/deployments/) | October 2023|
| No Trace Project (previously *Counter-Surveillance Resource Center*) | [www.notrace.how](https://www.notrace.org) | Research | October 2023 |
| Privacy International | [privacyinternational.org](https://privacyinternational.org) | Research | October 2023 |
| Shelter City | [sheltercity.org](https://sheltercity.org) | Emergency Support | October 2023
| Tactical Tech | [tacticaltech.org](https://tacticaltech.org) | Research | October 2023 |
| The Citizen Lab | [citizenlab.ca](https://citizenlab.ca) | Research // Auditing | October 2023 | 
| The Engine Room | [www.theengineroom.org](https://www.theengineroom.com) | Research | October 2023 |
