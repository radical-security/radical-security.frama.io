+++
title = "Situational Awareness"
weight = 2
tags = ["security-culture", "security-101", "surveillance"]
+++

One of the main bases of good security is being aware of what surrounds us, and what is happening around us. It is the skill, built over time, that allows us to detect real time threats and act accordingly.

There are a plethora of exercises that can prepare you for common situations, even though you cannot always prepare for everything. Before going into the exercises, let's do a quick theoretical introduction.

## Physical Surveillance 101

Most of the physical surveillance happens for 3 general reasons:
- Information Gathering (*next steps for the movement, future actions, information about specific militants (address, work, close people and comrades), meetings and members of an organization,*) - it is mainly used to understand who you are and what is the movement;
- Intimidation - the thought/feeling of being surveilled might be enough to discourage whatever plans you have;
- Process - the state might be following you to understand your schedules, to arrest you at a later time;

No matter what is the reason behind the surveillance, it is always essential to find the **balance** between continuing to act, taking precautionary measures and to not feel paranoid.

The general tips to be aware of your surroundings are:
- **Follow your Instinct** - If you feel that something is wrong / out of place, even when you cannot pinpoint why, you should follow your instincts and get away from that situation (*it might be a meeting you are going to that feels strange, the place where you are going to act that seems to have more police/security than when you scouted it...*);
- **Being aware of your surroundings** - In the common places of your life (home, university / workplace, movement spaces) try to memorize what exists there and find the things that feel out of place. Physical Surveillance has to start somewhere and these are some of the most common places to start.
  - Be aware of the people - People that seem out of place, people that are always there but don't interact with you, people that appear in multiple places of your life (in front of your house, at work, in a leftist space) that you don't know;
  - Be aware of the objects - Strange devices plugged into walls ([Ears and Eyes](https://www.notrace.how/earsandeyes/) is a really good project to present some of the surveillance technology being used against anarchist militants), cars and vans parked on the street regularly without anyone from your street/collective;
- **Create a communication plan** - If you are being actively surveilled, how will you communicate to your companions what is going on?
  - With whom will you communicate?
  - What will you say?
  - Will you do it covertly? Is there a specific phrase that you can use to not raise suspicion?
  - What is your response plan? Do you have a burner phone (for example) to communicate with key people? What are your steps after understanding that you are being surveilled?

**S**ituational **A**wareness (**SA**) is based on a cycle between:
- Observe;
- Decide;
- Act;

This only works if you know what to *Observe*, what to *Decide* to be able to *Act*.

There is a collection of common [Reactions](/security-101/risk-101/#reactions) on the page about [Understanding Risks](security-101/risk-101)

## Practical Training

Besides training for specific situations, you also need to train your own eyes to detect new information.

Some of these exercises come from [Protest Games](https://www.protestgames.org), a really interesting resource to find games and affinity building exercises for activists.

### Observation Skills

#### Object Game

One simple exercise to get used to observing and memorizing information is:
1. Someone gets 10-15 objects and lays them out on a table;
2. They give you 1 minute to memorize the objects in that table;
3. They cover the objects;
4. You tell them as many objects as you remember;

The first few rounds might be hard and you might only get some of the objects, but with time your eyes and brain will start to get better at memorizing them.

After a while, you might also want to try to memorize the conditions of the objects (such as color, marks, maker...).

#### People "Memory" Game

This exercise is really useful to create a collective sense of "the surveillor". It can be useful if multiple people feel that they are being surveilled and can describe the person, creating a clearer picture of who is watching and who is being whatched.

1. One person picks an image of someone (might be a family member or a random picture from the Web);
2. That person shows the picture to the group for a while (30 seconds to 1 minute);
3. Each person from the group tries to write down a description of the person;
4. You compare the descriptions between the group and then you show the image;

It is normal for people to notice different things, the main purpose of the exercise is to get you to notice on as many things as possible, and to be able to describe them clearly for other people.

#### Solo-Exercise - Peripheral Vision

Most of us walk looking at our screens, this creates a tunnel vision that will shorten what our eyes can actually see.

This is a simple exercise that you can do while waiting for someone, or while waiting for the train to arrive.

In order to be able to train your Peripheral Vision, you can:

1. Without moving your head, scan your surroundings from left to right, covering mostly what is right in front of you;
2. Describe what you saw;
3. Refocus and try to go a little further to what is far away from you, and more to the side;
4. Describe what you saw;
5. Repeat this process as many times as you can, trying to always go further to the back and to the sides;
6. When you get the hang of it, you can try to describe objects or people that only show up for a brief second on your side view;

#### Protest Scavenger Hunt

If you go to a protest with an affinity group but don't plan to do anything besides being there, you can use that time to train your observational skills.

1. In your group, decide who will be the "Treasure Finder"
2. The "Treasure Finder" will have 10-15 minutes to go into the march, find signs and banners and note them down;
3. The Scavengers will then write all of the "treasures" and try to take a picture of them (always ask for consent while photographing other demonstrators). You can also play this exercise without phones, noting down which treasures you found;
4. Everyone gets back together and changes the "Treasure Finder";

You can also play this game in other places, like at the Social Center's party or in the street;

#### Commuting

One easy and quick exercise you can do daily is to try and memorize who is on your *Public Transport* commute regularly (*extra points if you greet them and create solidarity with other members of our class*).

You can always complexify your task, trying to memorize where they leave the bus or what bag where they carrying.

### Little Brother

*Taken directly from [Protest Games](https://www.protestgames.org/index.html#Little%20Brother)*

Your aim is to take covert photos or film of the other players. If spotted taking footage, you must stop immediately. For footage to count, a player must be recognisable in it – either by clothes or by facial features, according to mutual agreement.

Play for at least half an hour at any kind of protest. Limit each recording to less than ten seconds for practicality and battery saving. At the end of the time limit, compare footage.

The player who took the most footage wins the Cop Award.

The player who appears in the most footage wins the Mask Up Prize, and any players who appear in no footage win the Covert Award.

If you win both the Cop and the Covert Award, apply to your nearest government intelligence agency or infosec division.

Advanced Mode: Play for a week. Expert Mode: Never stop playing.

### Exits and CCTVs

Another simple exercise that you can do most of the days, to train your Situational and Spatial Awareness is, while entering into a new building, memorize the Exit routes and try to find most of the CCTVs (this exercise is especially useful to train for Direct Actions).

### Role-play

Role-playing might sometimes be useful to put yourself into new scenarios that you hadn't thought before.

A simple exercise would be:
1. In a group, one person presents the scenario;
2. Everyone gets a role in the scenario;
3. You do the scenario;
4. Debrief, you talk about what happened, what could be different, what new thoughts came from the exercise;
5. *Optional - You might repeat this with new inputs, discovering new thoughts and practices*;

Some of the scenarios you might want to role-play are:


### Final notes

Most of these exercises, if done regularly, can improve your Situational Awareness by a lot. YOu should try to do them with comrades and share your progress and celebrate victories.

**Be aware that different presenting people does not necessarily mean that an undercover operation is going on, and that someone "dressed as an activist" might be an undercover police/informant**. This also applies to the way someone speaks (if they do not know the local activist lingo). You should, as an organizer, be aware that new people might need to adapt or not fit instantly. More resources on infiltrators will be shown especially on the [Organizational Security](/orgsec) section and on articles about Security Culture.
