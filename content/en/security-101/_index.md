+++
archetype = "chapter"
title = "Security 101"
weight = 1
+++

*Scroll down to the end to find the list of post under this chapter.*

---

## What is Security?

As Radical Militants, many of our ways of action either are "Illegal" (under the current system's moral compass, which is profoundly wrong) or in a "Grey Area".

Many times we think of Security when planning an action or protest and want to avoid Legal Repression. We need to replace that thought pattern, we need to think of Security as a way to **build trust** between comrades, open the doors to **do challenging actions** without being stopped before, **envision a world** Post-Surveillance Capitalism.

We are under surveillance, and we have generally a wrong understanding of what it might encompass. It is not about **only** stopping our actions, it is about:
- Understanding our relationships between each other;
- Discovering weaknesses that can be exploited later on to crush the movement;
- Understanding how we communicate, what specific language is used by whom and which group/collective;
- Understanding our tactics and what motivates/inspires us;

**Most** surveillance is either about *Intelligence Gathering* (either to prepare Legal Processes against people or to prepare for an upcoming action) or *Intimidation* (the sole thought of being under surveillance might (in some cases) be enough to dissuade someone from acting against an unjust system).

---

## Chapter Articles

List of posts and guides under this chapter:

{{% children sort="weight" %}}
