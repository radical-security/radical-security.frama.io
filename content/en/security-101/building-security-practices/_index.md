+++
title = "Building Security practices"
weight = 1
tags = ["security-culture", "security-101"]
+++


Building Security practices is hard, most of the times it is stressing and it is a common reaction to freeze or give up. It is also very common to feel an individualistic approach, where there is no community nor collective work being done. We want to challenge that view, therefore our guiding principles *(a description of each is below the list)* are:
1. **Failing better next time**;
2. **Security is a process, not a checklist**;
3. **Security is a collective care practice**;
4. **This is a learning space**;
5. **Open-Sourcing Security**;

## Failing better next times

The traditional western thought and learning process is based on either being able to do something or to not be able to perform it. This leads to people giving up on security or people thinking that they will never be able to understand it.

We want to purpose a "new" (*many other places and organizational cultures have been following this practice for years*) practice, based on knowing that we will eventually fail, and using each failure as an opportunity to actually learn, not only from textbooks or exercises but from experience.

This also means to celebrate our small victories in creating and supporting each other on the Security Culture process.

## Security is a process, not a checklist

You have probably been to a camp, or massive action, that had a "**Security Culture Guide**" (or something similar), that presented policies (or bullet points) to follow, without explanation or collective reasoning behind. Some of the policies may be fit for the specific context, while others might seem out of place (especially when multiple organizational practices and methods come together to converge on one action/camp). Most of the times it lacks an actual analysis of what surround us, and is unable to flow to the needs of everyone in the space and to the specific threat model.

The adherence of a good Security Culture comes from the collective feeling of ownership of that proposal, not from a checklist that someone wrote. It is also far better to explain the thinking behind the proposal, for new members that were not present.

Processes are evolving, they are not a "one time deal". We cannot spend 2 days working on our Security Culture, drafting protocols and guides and then proceeding to the next task. Building a Security Culture means working constantly (as a collective) to address bad practices, find new vulnerabilities, and creating ways to get stronger.

## Security is a collective care practices

Security is about many different things. It is about being able to build a better world, fighting state surveillance and being able to actually plan for a future and present. It is about protecting comrades and compas, not endangering them. It is about allowing at risk people to join you, while taking care of their needs.

Security is sometimes used as a way to either silence or shut-down people, we need to work on that. We **cannot** build a transformative Security Culture where Patriarchal behavior is used in justification of "Security".

I recommend everyone to read the following article, it was written driven by an unfortunate situation but it is essential to read. It is [Why Mysoginists Make Great Informants](https://www.akpress.org/feminisms-in-motion.html) by [Courtney Desiree Morris](https://www.courtneydesireemorris.com/).

## This is a learning space

As with most projects around security, this is a learning space for you and for the project. I (*Larus Argentatus*) don't know everything, will make eventually mistakes and want to learn from them. I come from a specific Organizational Culture that might reflect on some of the things I write. At the same time, I try to read from other collectives and comrades that have developed strategies around Security and Building Security Culture. I want to learn about topics that I know and don't know and I hope that you feel the same way.

## Open-Sourcing Security

In software development there are two major approaches:
- Open Source;
- Proprietary / Closed Source;

The biggest difference between them is that in Open Source software anyone can **check** the code, **build** the application and discover **security** vulnerabilities, while with Proprietary software only the company developing the software has access (at least lawful access) to the source code.

Most of the Security related software and protocols are open-source for a simple reason, more people looking at the code generally means that security vulnerabilities are easier to detect and you do not have to necessarily trust the developers. [^1]

The same approach can be done with our Security Practices, *security trough obscurity* generally doesn't work and you will be spending much more energy trying to keep it secret than actually protecting yourself / the collective.

As such, we will also recommend **Open-Source** Software that is actively maintained when reccomending software and new tools.

[^1]: On the other end of the spectrum, Proprietary Protocols are just seen by those that develop them, this obviously doesn't work very well. You can look at the [TETRA:BURST](https://www.tetraburst.com/) disclosure to see what happens when no one looks at a protocol besides the people that created it.
