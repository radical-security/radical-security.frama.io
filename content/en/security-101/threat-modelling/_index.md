+++
title = "Threat Modeling"
weight = 4
tags = ["security-culture", "security-101", "threat-model"]
+++

We live in complex times (as most people that engaged in Radical Action lived). We live in times where change has to be done by us before everything changes against us.

The stakes are high, repression is expanding, not only on the physical realm, but also on the digital one.

Governments, fossil fuel companies and the far right mobilize against us and try to intimidate us.

That is why we must create good **Threat Models** to inform our security practices.

## What is a Threat Model?

A **Threat Model** is an analysis tool that answers:
1. **What** are we protecting;
2. **Why** are we protecting it;
3. **From whom** are we protecting it;

The **what**, **why** and **from whom** informs what do we need to do and sets up the bar for revolutionary discipline in our day to day life, concerning security.

It is a structured approach to identify and prioritize potential threats and creating useful and functional mitigations to neutralize those threats.

When doing a threat modeling, you should also take into consideration your current structure, how you communicate internally and how do you communicate to the general public.

## Creating a Threat Model

Find time to do this collectively since the threat model is the analysis that justifies your security practices.

Threat modeling should be done before major actions for specific practices and threats, but you should also have a threat model for your collective / organization, it should be updated regularly and known by all the members.

**When starting, you should begin from the inside out.** Therefore, start by answering the **what**. Some of the main guiding questions around it are:
- Are there meeting places that need to be kept hidden?
- Where are the objects (speakers, banners, megaphones ...) of the collective stored?
- Are we trying to hide the identity of the members of the collective?
- What are our next steps? Are they public?

Afterwards, to create common understanding on why there are security practices. collectively discuss **why** are you protecting that information, you might discover that some of it is critical, while other information is already public.

Using the input of the exercises from [Understanding Risk](/security-101/risk-101) ([Risks Table](/security-101/risk-101/#risks-table) and [Risk Matrix](/security-101/risk-101/#risk-matrix)) try to understand who would be able to turn those Risks into a reality. Those are the people that you are protecting from. It is advised to go as specific as possible (*is it a Working Group from the Local Law Enforcment Agency? is it a specific Far-Right Organization, what is their common modus operandi?*).
