+++
title = "Understanding Risk"
weight = 3
tags = ["security-culture", "security-101", "risk", "threat-model"]
+++

Risk can generally be presented with the following formula:

{{< math align="center" >}}
$Risk=\frac{Threats \times Vulnerabilities}{Capacities}$
{{< /math >}}

Where:
- **Risk** is "the possibility of an event resulting in damage (*psychological, legal, financial, personal, organizational, physical*)";
- **Threats** are the "indication, suspicion or declaration of intention to create damage (*psychological, legal, financial, personal, organizational, physical*)";
- **Vulnerabilities** are the "factors that inhibit damage or that exponentiate the final result";
- **Capacities** are the "factors (*resources, technological capacities, mental state, support people and specific abilities) that mitigate the possibility of damage or reduce the impact of it";

When talking about security measures, there are 3 major approaches/strategies, these approaches inform us on what we should focus on (based on the formula above):
- **Acceptance** - focused in **reducing** threats, creating consensus in society about the work you are doing and negotiating with the other actors in society to neutralize those that might become enemies (reducing *Threats*);
- **Dissuasion** - focused in **intimidating** threats, using legal, public relations or political repercussion to those that threaten your movement. *This approach works mostly when you have power or strong allies*;
- **Protection** - focused on **protecting** your collective and actions, with a strong emphasis in Security Culture, functional and informed Operational Security and robust Organizational Security to deal with repression, surveillance and infiltration (improving *Capacities*);

In general, the strategy that we follow is the **Protection** strategy, even though that different moments, and specific actions might have a different strategy underlying it.

## Reactions

One of the biggest necessities to understand when talking about [Situational Awareness](/security-101/situational-awareness/) and [Risk](/security-101/risk-101) is how will you react (*Decide*) to what you observe. In general, there are 5 common reactions to threats:

|Reaction|Description|
|:---:|:---|
|**Freezing**|Stopping what you are doing, not knowing how to react, not knowing how to communicate with other people about the threat.|
|**Ignoring**|Continuing your work without acknowledging the threat, belittle the threat, not acting.|
|**Coping Mechanisms**|Being aware of the threat even though you don't act against it. Using other ways to deal with it (*ex. Alcohol*).|
|**Analyzing**|In the moment of the threat, being able to communicate and make space to analyze the situation with other members/comrades.|
|**Calmness**|Before the threat, analyzing possible threats and creating Response plans to act upon if they materialize.|

There are plenty reasons why someone might react, from the mental state, to energy in the moment, material conditions... including **previous** training, experience and preparations.

{{% notice style="warning" title="Tips for Analysis" icon="user-ninja" %}}
To make it easier to understand the Analysis of a Threat, you can follow the following list:
(*This analysis might start as an individual practice and then be shared with the collective.*)

1. Why does this **Threat** exist?
2. What is the history of similar Threats? (*If you were ordered to go to court with an accusation, what happened to other activists that had that same accusation?*);
3. Who is affected by this **Threat** (*are you going to meet with an affinity group? is it the whole collective? is it a specific group of people?...*);
4. What happens when the threat becomes real? (*what is the impact? does an action collapse? will 10 people be arrested if "X" happens?*);

**Extra** - You might want to think about resiliency and the *Bus Factor* (if you were run over by a bus today, what would happen to the tasks you have? are there other informed people that can make decisions and act on if you are missing?).

{{% /notice %}}

## Final notes

Based on understanding Risks, Security Strategies and common Reactions, we purpose some exercises you might do (personally or in collective).

### Risks Table

You might do a table like this for each action / task that you have. Afterward the people from the collective might share them and analyze them collectively, detecting common risks and necessities.

*Example Table*

| Risks | Vulnerabilities | Existing Capacities | Missing Capacities |
| :-------------: | :------------- | :------------- | :------------- |
| Being Kidnapped | Living Alone | Good Home Security (alarms, fence, cameras) // Neighbors that are awake during the night // I have a place to stay if there is a high **Threat** | Getting a guard dog // One person knows where I am trough the day // Creating the habit of communicating with one comrade twice a day to tell them I am okay |
| Arrest | Fake Accusations | I know my rights // I have a lawyer briefed and ready to act // My home and office do not have incriminating documents in case of searches after an arrest | Memorize my lawyers number in case they apprehend my phone |
| Arrest | Specific medical condition | I have medication to deal with it | Always carry the medication |

### Risk Matrix

Besides understanding the risks and what we might to if something happens, it is always to understand the actual probability of the risk to happen and the impact if that materializes.

To do that, we might use the *Risk Matrix*
||||||||
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
||Very High|🟩|🟨|🟨|🟥|⬛|
||High|🟩|🟩|🟨|🟥|🟥|
|Impact|Medium|🟩|🟩|🟨|🟨|🟨|
||Low|🟩|🟩|🟩|🟩|🟨|
||Very Low|🟩|🟩|🟩|🟩|🟩|
|||Very Low|Low|Medium|High|Very High|
|||||Probability|||

With the Risk Matrix done, we can understand and focus our energies on addressing the risks in top-right corner first, and then proceed to mitigate the other ones.
