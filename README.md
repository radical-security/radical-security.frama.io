# Radical Security

<img src="static/images/logo.png" width="25%"></img>

## What is it?

Radical Security is a project run currently by 1 activist, security trainer and security officer with the alias Larus Argentatus (if you want to contribute, don’t be afraid to reach out). It was created to aggregate and produce new knowledge for Radical (*as in to the root of the problem*) Activists, Militants and Organizers.

It appeared from the necessity of creating content that would encompass multiple threat models and at the same time be realist on what are the risks. The current online mainstream guides are not enough for us (from the ones made for Investigative Journalists to those made to Human Rights Defenders).

It is organized in 3 main sections:
- Security 101
- Operational Security
- Organizational Security

## How can you contribute

There are many ways to contribute and I hope that there is one for you.

### Create new content

We are actively seeking for new content, new ideas and additions to the current content. To do that:

1. Install [git](https://git-scm.com/downloads);
2. Clone this repository `git clone https://framagit.org/radical-security/radical-security.frama.io.git`;
3. Add this repository to your git connection `git remote add <NAME> https://framagit.org/radical-security/radical-security.frama.io.git`;
4. Add, or alter the content inside `/content`;
5. Commit your changes and explain them `git commit -m "<EXPLANATION>"`;
6. Create a [framagit](https://framagit.org) account;
7. Push your changes to be approved `git push --set-upstream <NAME> main`;

You can also send suggestions by e-mail at `radical-security [at] riseup.net`.

### Ask for specific content

You can ask for specific content by opening an `Issue`.

1. Create a [framagit](https://framagit.org) account;
2. Open an `Issue` on the [Issue](https://framagit.org/radical-security/radical-security.frama.io/-/issues) tab;
3. Describe what content do you want, give as many information as possible;

You can also send requests by e-mail at `radical-security [at] riseup.net`.

### Share it

Share the content with comrades and compas. The maintainer is also in Mastodon [@infosec.exchange@larusargentatus](https://infosec.exchange/@larusargentatus).

### Send your feedback

Send it to `radical-security [at] riseup.net`.
